package com.daly.tabsexample

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class TabAdapter(fm: FragmentManager?, var fragments: ArrayList<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(p0: Int): Fragment {
        return fragments[p0]

    }

    override fun getCount(): Int {
        return fragments.size

    }

    override fun getPageTitle(position: Int): CharSequence? {

        return when (position) {
            0 ->
                "First Tab"
            1 ->
                "Second Tab"
            2 ->
                "Three Tab"
            else ->
                "Three Tab"

        }
    }
}